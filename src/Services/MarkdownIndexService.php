<?php

namespace Drupal\markdown_index\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountInterface;

/**
 * Service description.
 */
class MarkdownIndexService {

  /**
   * Regular expression for markdown files - this should be constant.
   */
  const REGEX_PATTERN = '/.*\.md/';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  public ConfigFactory $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  public AccountInterface $currentUser;

  /**
   * Constructs a MarkdownIndexService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface|null $current_user
   *   The current user.
   */
  public function __construct(ConfigFactory $config_factory, AccountInterface $current_user = NULL) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * Get the list of files in a folder.
   *
   * @param string $folder
   *   The folder to search.
   *
   * @return array
   *   The list of files.
   */
  public function rsearch(string $folder): array {
    $dir = new \RecursiveDirectoryIterator($folder);
    $ite = new \RecursiveIteratorIterator($dir);
    $files = new \RegexIterator($ite, self::REGEX_PATTERN, \RegexIterator::GET_MATCH);
    $fileList = [];
    foreach ($files as $file) {
      $fileList = array_merge($fileList, $file);
    }
    return $fileList;
  }

  /**
   * Get the path for a folder.
   *
   * @param string $folder
   *   The folder to get the path for.
   *
   * @return string
   *   The path.
   */
  public function getPath(string $folder): string {

    $docroot = $_SERVER['DOCUMENT_ROOT'];

    // Project root is one level up from docroot.
    $docroot_ar = explode('/', $docroot);
    array_pop($docroot_ar);
    $project_root = implode('/', $docroot_ar);

    switch ($folder) {

      case "files_root":
        $return = $project_root;
        break;

      case "files_contrib":
        $return = $docroot . '/modules/contrib';
        break;

      case "files_custom":
        $return = $docroot . '/modules/custom';
        break;

      case "files_vendor":
        $return = $docroot . '/../vendor';
        break;

      default:
        $return = "";
        break;

    }

    return $return;

  }

}
