<?php

namespace Drupal\markdown_index\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\markdown_index\Services\MarkdownIndexService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Markdown Index form.
 */
class MarkdownIndexSelectForm extends FormBase {

  /**
   * Markdown Index helper service.
   *
   * @var \Drupal\markdown_index\Services\MarkdownIndexService
   */
  protected MarkdownIndexService $miService;

  /**
   * Construct.
   *
   * @param \Drupal\markdown_index\Services\MarkdownIndexService $markdown_index_service
   *   Markdown index service.
   */
  public function __construct(MarkdownIndexService $markdown_index_service) {
    $this->miService = $markdown_index_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('markdown_index.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_index_markdown_index_select';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $markup = '<p>Use this form select a markdown file to view.</p>';
    if ($this->miService->currentUser->hasPermission('administer markdown_index configuration')) {
      $markup .= '<p>To update the folders included in this dropdown, adjust the <a href="/admin/config/system/markdown-index">Markdown Index Settings.</a></p>';
    }
    $form['description'] = [
      '#markup' => $markup,
    ];

    // Initialize our options.
    $markdown_files_options = [];

    // Load the configuration for this module.
    $config = $this->miService->configFactory->getEditable('markdown_index.settings')->getRawData();

    if (!empty($config['mapping'])) {

      // Loop over the config data and add files to the options.
      foreach ($config['mapping'] as $key => $value) {
        // If checked on/true.
        if ($value) {
          // We will use recursion for all folder but the root.
          if ($key != 'files_root') {
            $files = $this->miService->rsearch($this->miService->getPath($key));
            $markdown_files_options = array_merge(array_values($markdown_files_options), array_values($files));
          }
          else {
            $glob_pattern = getcwd() . '/../*.md';
            $markdown_files_options = [];
            foreach (glob($glob_pattern) as $file) {
              $markdown_files_options[] = $file;
            }
          }
        }
      }
    }

    /*
     * @todo would be nice to have option groups.
     */

    // Sort the options, will need to adjust if in groups.
    asort($markdown_files_options);

    // Since asort() maintains keys, need to re-key here.
    $markdown_files_options = array_values($markdown_files_options);

    // Get the file path for a selected markdown file to view.
    $file_index = \Drupal::request()->query->get('file_index') ?? NULL;

    if ($file_index && $file_index <= count($markdown_files_options)) {
      // Since we add "Select" as the first option, we need to subtract 1.
      if ($markdown = file_get_contents($markdown_files_options[$file_index - 1])) {
        $markdown = '<pre>' . $markdown . '</pre>';
      }
    }

    /*
     * @todo adjust for "prettier" options.
     */

    array_unshift($markdown_files_options, '-- Select --');

    $form['markdown_file'] = [
      '#type' => 'select',
      '#title' => $this->t('Markdown Files'),
      '#options' => $markdown_files_options,
      '#default_value' => $file_index ?? 0,
      '#required' => FALSE,
      '#attributes' => ['onchange' => 'this.form.submit();'],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('View File'),
      '#attributes' => [
        'style' => 'display:none;',
      ],
    ];

    if (!empty($markdown)) {
      $form['markdown_display'] = [
        '#markup' => $markdown,
        '#weight' => 100,
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_index = $form_state->getValue('markdown_file');
    $form_state->setRedirectUrl(Url::FromRoute('markdown_index.report', ['file_index' => Html::escape((int) $file_index)]));
  }

}
