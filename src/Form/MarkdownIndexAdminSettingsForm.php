<?php

namespace Drupal\markdown_index\Form;

use Drupal\markdown_index\Services\MarkdownIndexService;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Markdown Index settings for this site.
 */
class MarkdownIndexAdminSettingsForm extends ConfigFormBase {

  /**
   * Markdown Index Service.
   *
   * @var \Drupal\markdown_index\Services\MarkdownIndexService
   */
  protected MarkdownIndexService $miService;

  /**
   * MarkdownIndexAdminSettingsForm constructor.
   *
   * @param \Drupal\markdown_index\Services\MarkdownIndexService $markdown_index_service
   *   Markdown Index Service.
   */
  public function __construct(MarkdownIndexService $markdown_index_service) {
    $this->miService = $markdown_index_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('markdown_index.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_index_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['markdown_index.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $markup = '<p>Use this form to determine what folders are included in the Markdown Index report.</p>';
    if ($this->miService->currentUser->hasPermission('access markdown_index report')) {
      $markup .= '<p>Click here to view the <a href="/admin/reports/markdown-index">Markdown Index Report.</a></p>';
    }
    $form['description'] = [
      '#markup' => $markup,
    ];
    $form['files_root'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Project Root Files'),
      '#default_value' => $this->config('markdown_index.settings')->get('mapping.files_root'),
      '#description' => $this->miService->getPath('files_root'),
    ];
    $form['files_contrib'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Contrib Module Files'),
      '#default_value' => $this->config('markdown_index.settings')->get('mapping.files_contrib'),
      '#description' => $this->miService->getPath('files_contrib'),
    ];
    $form['files_custom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom Module Files'),
      '#default_value' => $this->config('markdown_index.settings')->get('mapping.files_custom'),
      '#description' => $this->miService->getPath('files_custom'),
    ];
    $form['files_vendor'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Vendor Files'),
      '#default_value' => $this->config('markdown_index.settings')->get('mapping.files_vendor'),
      '#description' => $this->miService->getPath('files_vendor'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('markdown_index.settings')
      ->set('mapping.files_root', $form_state->getValue('files_root'))
      ->set('mapping.files_contrib', $form_state->getValue('files_contrib'))
      ->set('mapping.files_custom', $form_state->getValue('files_custom'))
      ->set('mapping.files_vendor', $form_state->getValue('files_vendor'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
