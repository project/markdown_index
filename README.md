# Markdown Index

Generates an index of markdown files in your contrib folder for reference.

## Installation

Install the module as you would any other module.
- `composer require drupal/markdown_index`
- `drush en markdown_index`

## Post installation

Navigate to the settings form to include the folders that you want included in
the report.
- Configuration -> System -> Markdown Index Settings
- /admin/config/system/markdown-index

After enabling the module see "Markdown Index" in the "Reports" menu.
- Reports -> Markdown Index Report
- /admin/reports/markdown-index

## ToDo list / Enhancements
- Better readme up to Drupal standards
- Markdown module integration
- Store results in database table
